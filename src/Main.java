public class Main {
    public static void main(String[] args) {

        Medienbibliothek medien = new Medienbibliothek();
        Genre genre = new Genre("action");
        Urheber urheber = new Urheber("Auch matteo", "Bombardelli", "Gritsch GmbH");
        eBooks buch = new eBooks("Mark auf dem Autodach", "11.11.2024", "Ein einmaliges Erlebnis",genre,12, 123123, "Philipps", "Matteo", urheber );
        Filme Film = new Filme("Mark auf dem Autodach (der Film)", "11.11.2024", "Mark im Kino!", genre, "Matteo", 90, true, false, urheber);

        medien.hinzufügen(buch);
        medien.hinzufügen(Film);


        medien.alleMedienausgeben();

    }
}
