public class Filme extends Medium {

    private String regisseur;
    private int spielzeit;

    private boolean UHD;
    private boolean HD;

    public Filme(String titel, String erscheinungsdatum, String kommentar, Genre genre,String regisseur, int spielzeit, boolean UHD, boolean HD, Urheber urheber){

        super(titel, erscheinungsdatum, kommentar, genre, urheber);

        this.regisseur = regisseur;
        this.spielzeit = spielzeit;
        this.UHD = UHD;
        this.HD = HD;




    }

    public String getRegisseur() {
        return regisseur;
    }

    public void setRegisseur(String regisseur) {
        this.regisseur = regisseur;
    }

    public int getSpielzeit() {
        return spielzeit;
    }

    public void setSpielzeit(int spielzeit) {
        this.spielzeit = spielzeit;
    }

    public boolean isUHD() {
        return UHD;
    }

    public void setUHD(boolean UHD) {
        this.UHD = UHD;
    }

    public boolean isHD() {
        return HD;
    }

    public void setHD(boolean HD) {
        this.HD = HD;
    }

    @Override
    public void anzeigen() {
        super.anzeigen();
        System.out.println("Regisseur -> " + this.getRegisseur() + " Spielzeit -> " + this.getSpielzeit() + " UHD -> " + this.isUHD() +" HD -> "+ this.isHD()  );
    }
}
