public class Medium {
    private String titel;
    private String erscheinungsdatum;
    private String kommentar;

    private Urheber urheber;
    private Genre genre;

             public Medium(String titel, String erscheinungsdatum, String kommentar, Genre genre, Urheber urheber){

                 this.titel = titel;
                 this.erscheinungsdatum = erscheinungsdatum;
                 this.kommentar = kommentar;
                 this.genre = genre;
                this.urheber = urheber;



             }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public String getErscheinungsdatum() {
        return erscheinungsdatum;
    }

    public void setErscheinungsdatum(String erscheinungsdatum) {
        this.erscheinungsdatum = erscheinungsdatum;
    }

    public String getKommentar() {
        return kommentar;
    }

    public void setKommentar(String kommentar) {
        this.kommentar = kommentar;
    }


    public void setGenre(Genre genre) {

                 this.genre = genre;
    }


    public Urheber getUrheber() {
        return urheber;
    }

    public void setUrheber(Urheber urheber) {
        this.urheber = urheber;
    }

    public void anzeigen(){

        System.out.println("Medium: Titel -> " + this.getTitel() + " Erscheinungsdatum -> " + this.getErscheinungsdatum() + " Kommentar -> "+ getKommentar() + " Genre -> " + genre.getName() + " Urheber -> " + urheber.toString());

    }


}
