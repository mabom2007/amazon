public class eBooks extends Medium  {

private String Autor;
private String Auflage;
private int ISBN;
private int Seitenanzahl;




    public eBooks(String titel, String erscheinungsdatum, String kommentar, Genre genre, int seitenanzahl, int ISBN, String Auflage, String autor, Urheber urheber){

        super(titel, erscheinungsdatum, kommentar, genre, urheber);

        this.ISBN = ISBN;
        this.Auflage =  Auflage;
        this.Autor = autor;
        this.Seitenanzahl = seitenanzahl;
    }




    public String getAutor() {
        return Autor;
    }

    public void setAutor(String autor) {
        Autor = autor;
    }

    public String getAuflage() {
        return Auflage;
    }

    public void setAuflage(String auflage) {
        Auflage = auflage;
    }

    public int getISBN() {
        return ISBN;
    }

    public void setISBN(int ISBN) {
        this.ISBN = ISBN;
    }

    public int getSeitenanzahl() {
        return Seitenanzahl;
    }

    public void setSeitenanzahl(int Seitenanzahl) {
        this.Seitenanzahl = Seitenanzahl;
    }



    @Override
    public void anzeigen() {
        super.anzeigen();
        System.out.println("Autor -> " + this.getAutor() + " Auflage -> " + this.getAuflage() + " ISBN -> " + this.getISBN() +" Seitenanzahl -> " + this.getSeitenanzahl());
    }




}
